package com.jungly.gridpasswordview;

/**
 * PasswordView
 *
 * @author Jungly
 * @mail jungly.ik@gmail.com
 * @date 15/3/21 16:20
 */
interface PasswordView {
    /**
     * getPassWord
     *
     * @return String
     */
    String getPassWord();

    /**
     * clearPassword
     */
    void clearPassword();

    /**
     * setPassword
     *
     * @param password
     */
    void setPassword(String password);

    /**
     * setPasswordVisibility
     *
     * @param visible
     */
    void setPasswordVisibility(boolean visible);

    /**
     * togglePasswordVisibility
     */
    void togglePasswordVisibility();

    /**
     * setOnPasswordChangedListener
     *
     * @param listener
     */
    void setOnPasswordChangedListener(GridPasswordView.OnPasswordChangedListener listener);

    /**
     * setPasswordType
     *
     * @param passwordType
     */
    void setPasswordType(PasswordType passwordType);
}
