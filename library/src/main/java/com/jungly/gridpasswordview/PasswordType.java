package com.jungly.gridpasswordview;

/**
 * PasswordType
 *
 * @author Jungly
 * jungly.ik@gmail.com
 * 5/3/21 16:47
 */
public enum PasswordType {
    /**
     * NUMBER, TEXT, TEXTVISIBLE, PATTERN_NULL, PASSWORD
     */
    NUMBER, TEXT, TEXTVISIBLE, PATTERN_NULL, PASSWORD
}