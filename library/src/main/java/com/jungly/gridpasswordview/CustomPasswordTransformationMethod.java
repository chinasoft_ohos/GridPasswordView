package com.jungly.gridpasswordview;

/**
 * 默认'●'
 *
 * @author Jungly
 * mail: jungly.ik@gmail.com
 */
public class CustomPasswordTransformationMethod {
    private final String transformation;

    /**
     * constructor
     *
     * @param transformation
     */
    public CustomPasswordTransformationMethod(String transformation) {
        this.transformation = transformation;
    }

    private class PasswordCharSequence implements CharSequence {
        private final CharSequence mSource;

        /**
         * constructor
         *
         * @param source
         */
        public PasswordCharSequence(CharSequence source) {
            mSource = source;
        }

        @Override
        public int length() {
            return mSource.length();
        }

        @Override
        public char charAt(int index) {
            return transformation.charAt(0);
        }

        @Override
        public CharSequence subSequence(int start, int end) {
            return mSource.subSequence(start, end);
        }
    }
}