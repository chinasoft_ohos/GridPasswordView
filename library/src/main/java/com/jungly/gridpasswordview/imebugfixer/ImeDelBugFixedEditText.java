package com.jungly.gridpasswordview.imebugfixer;

import ohos.agp.components.AttrSet;
import ohos.agp.components.TextField;
import ohos.app.Context;

public class ImeDelBugFixedEditText extends TextField {
    /**
     * constructor
     *
     * @param context
     */
    public ImeDelBugFixedEditText(Context context) {
        super(context);
    }

    /**
     * constructor
     *
     * @param context
     * @param attrs
     */
    public ImeDelBugFixedEditText(Context context, AttrSet attrs) {
        super(context, attrs);
    }

    /**
     * constructor
     *
     * @param context
     * @param attrs
     * @param defStyle
     */
    public ImeDelBugFixedEditText(Context context, AttrSet attrs, String defStyle) {
        super(context, attrs, defStyle);
    }

    public interface OnDelKeyEventListener {
        /**
         * onDeleteClick
         */
        void onDeleteClick();
    }
}