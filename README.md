# GridPasswordView

#### 项目介绍

- 项目名称：GridPasswordView
- 所属系列：OpenHarmony的第三方组件适配移植
- 功能：GridPasswordView密码视图，类似于微信应用和支付宝应用中的支付密码视图
- 项目移植状态：主功能完成
- 调用差异：系统软键盘无法拉起，未能找到控制系统软键盘的Api；部分继承自源码的方法未实现，原因：1、OpenHarmony不支持这些Api；2、这些方法在原项目中只是用于空实现，没有实际意义；如这些方法：setBackground、 setBackgroundColor、 setBackgroundResource、 setBackgroundDra等。
- 开发版本：sdk6，DevEco Studio 2.2 Beta1
- 基线版本：Releases V0.3

#### 项目演示

<img src="https://gitee.com/chinasoft_ohos/GridPasswordView/raw/master/gridpasswordview.gif"></img>

#### 安装教程

 1.在项目根目录下的build.gradle文件中，

  ```
 allprojects {
     repositories {
         maven {
             url 'https://s01.oss.sonatype.org/content/repositories/releases/'
         }
     }
 }
  ```
  2.在entry模块的build.gradle文件中，

   ```
   dependencies {
      implementation('com.gitee.chinasoft_ohos:GridPasswordView:1.0.0')
      ......
   }
   ```
 在sdk6，DevEco Studio2.2 Beta1下项目可直接运行
 如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件,
 并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明

1. GridPasswordView是一个密码输入功能的界面。
```xml
         <com.jungly.gridpasswordview.GridPasswordView
                ohos:id="$+id:gridviewpassword6"
                ohos:height="match_content"
                ohos:width="match_parent"
                ohos:left_margin="15vp"
                ohos:right_margin="15vp"
                ohos:top_margin="10vp"
                app:gpv_grid_color="#ff0000"
                app:gpv_line_width="6"
                app:gpv_password_length="6"
                app:gpv_password_transformation="$"
                app:gpv_show_password="true"
                app:gpv_text_size="30"
                />
```
2.使用方法
```xml
      设置边框的颜色
      app:gpv_grid_color="#ff0000"
      设置边框宽度
      app:gpv_line_width="6"
      设置密码的个数
      app:gpv_password_length="6"
      设置密码显示样式
      app:gpv_password_transformation="$"
      设置密码明文密文显示
      app:gpv_show_password="true"
      设置字体大小
      app:gpv_text_size="30"
      getPassWord()  获取GridPasswordView输入的密码
      clearPassword() 清理GridPasswordView控件中的密码
      togglePasswordVisibility()设置GridPasswordView控件中密码是否明文显示
      setPasswordType()设置GridPasswordView控件输入类型
      setOnPasswordChangedListener(OnPasswordChangedListener listener)监听GridPasswordView控件密码的输入变化
```
#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异

#### 版本迭代
- 1.0.0

#### 版权和许可信息


```
Copyright 2015 jungly

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```