/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.jungly.gridpasswordview;

import ohos.agp.components.*;
import ohos.app.Context;

import java.util.List;

public class ToolbarAdapter extends BaseItemProvider {
    private List<String> mColorList;
    private Context context;

    /**
     * constructor
     *
     * @param context
     * @param list
     */
    public ToolbarAdapter(Context context, List<String> list) {
        this.mColorList = list;
        this.context = context;
    }

    @Override
    public int getCount() {
        return mColorList == null ? 0 : mColorList.size();
    }

    @Override
    public Object getItem(int i) {
        if (mColorList != null && i >= 0 && i < mColorList.size()) {
            return mColorList.get(i);
        }
        return null;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public Component getComponent(int position, Component convertComponent, ComponentContainer componentContainer) {
        final Component cpt;
        if (convertComponent == null) {
            cpt = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_item_menu_list, null, false);
        } else {
            cpt = convertComponent;
        }
        String color = mColorList.get(position);

        Text text = (Text) cpt.findComponentById(ResourceTable.Id_text_item);
        text.setText(color);

        return cpt;
    }
}
