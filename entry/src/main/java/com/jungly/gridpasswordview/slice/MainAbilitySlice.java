package com.jungly.gridpasswordview.slice;

import com.jungly.gridpasswordview.GridPasswordView;
import com.jungly.gridpasswordview.PasswordType;
import com.jungly.gridpasswordview.ResourceTable;
import com.jungly.gridpasswordview.ToolbarAdapter;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.components.element.StateElement;
import ohos.agp.window.dialog.*;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import java.util.ArrayList;
import java.util.List;

public class MainAbilitySlice extends AbilitySlice {
    static final HiLogLabel label = new HiLogLabel(HiLog.LOG_APP, 0x00201, "MainAbilitySlice");
    private static final int SWITCH_CORNER_RADIUS = 50;
    private GridPasswordView mGridPasswordView;
    private GridPasswordView mGridPasswordView2;
    private GridPasswordView mGridPasswordView3;
    private GridPasswordView mGridPasswordView8;
    private Text mText;
    private Text mTextStyle;
    private boolean isFirst = true;
    private String firstPwd;
    private boolean password_type_number = true;
    private Switch mSwitch;
    private PopupDialog mPopupWindow;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        mGridPasswordView = (GridPasswordView) findComponentById(ResourceTable.Id_gridviewpassword);
        mGridPasswordView2 = (GridPasswordView) findComponentById(ResourceTable.Id_gridviewpassword_2);
        mGridPasswordView3 = (GridPasswordView) findComponentById(ResourceTable.Id_gridviewpassword_3);

        mSwitch = (Switch) findComponentById(ResourceTable.Id_switch_pass);
        setSwitchStyle(mSwitch);
        mGridPasswordView8 = (GridPasswordView) findComponentById(ResourceTable.Id_gridviewpassword8);
        mText = (Text) findComponentById(ResourceTable.Id_change_psw);

        mTextStyle = (Text) findComponentById(ResourceTable.Id_change_psw_style);
        mGridPasswordView8.setOnPasswordChangedListener(new GridPasswordView.OnPasswordChangedListener() {
            @Override
            public void onTextChanged(String psw) {
                if (psw.length() == mGridPasswordView8.DEFAULT_PASSWORD_LENGTH && isFirst) {
                    mGridPasswordView8.clearPassword();
                    isFirst = false;
                    firstPwd = psw;
                } else if (psw.length() == 6 && !isFirst) {
                    if (psw.equals(firstPwd)) {
                        isFirst = true;
                    } else {
                        mGridPasswordView8.clearPassword();
                        firstPwd = psw;
                    }
                }
            }

            @Override
            public void onInputFinish(String psw) {
            }
        });

        mText.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                showListPop();
            }
        });

        mTextStyle.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (password_type_number) {
                    password_type_number = false;
                    mTextStyle.setText("数字");
                    mGridPasswordView3.setPasswordType(PasswordType.NUMBER);
                } else {
                    password_type_number = true;
                    mTextStyle.setText("字母");
                    mGridPasswordView3.setPasswordType(PasswordType.TEXT);
                }
            }
        });

        mSwitch.setCheckedStateChangedListener(new AbsButton.CheckedStateChangedListener() {
            @Override
            public void onCheckedChanged(AbsButton absButton, boolean b) {
                if (b) {
                    mGridPasswordView3.setShowPassword(false);
                } else {
                    mGridPasswordView3.setShowPassword(true);
                }
            }
        });
    }

    private StateElement trackElementInit(ShapeElement on, ShapeElement off) {
        StateElement trackElement = new StateElement();
        trackElement.addState(new int[]{ComponentState.COMPONENT_STATE_CHECKED}, on);
        trackElement.addState(new int[]{ComponentState.COMPONENT_STATE_EMPTY}, off);
        return trackElement;
    }

    private StateElement thumbElementInit(ShapeElement on, ShapeElement off) {
        StateElement thumbElement = new StateElement();
        thumbElement.addState(new int[]{ComponentState.COMPONENT_STATE_CHECKED}, on);
        thumbElement.addState(new int[]{ComponentState.COMPONENT_STATE_EMPTY}, off);
        return thumbElement;
    }

    private void setSwitchStyle(Switch switchStyle) {
        ShapeElement elementThumbOn = new ShapeElement();
        elementThumbOn.setShape(ShapeElement.OVAL);
        elementThumbOn.setRgbColor(RgbColor.fromArgbInt(0xFF4aac4e));
        elementThumbOn.setCornerRadius(SWITCH_CORNER_RADIUS);

        // 关闭状态下滑块的样式
        ShapeElement elementThumbOff = new ShapeElement();
        elementThumbOff.setShape(ShapeElement.OVAL);
        elementThumbOff.setRgbColor(RgbColor.fromArgbInt(0xFFFFFFFF));
        elementThumbOff.setCornerRadius(SWITCH_CORNER_RADIUS);

        // 开启状态下轨迹样式
        ShapeElement elementTrackOn = new ShapeElement();
        elementTrackOn.setShape(ShapeElement.RECTANGLE);
        elementTrackOn.setRgbColor(RgbColor.fromArgbInt(0xffcde6ce));
        elementTrackOn.setCornerRadius(SWITCH_CORNER_RADIUS);

        // 关闭状态下轨迹样式
        ShapeElement elementTrackOff = new ShapeElement();
        elementTrackOff.setShape(ShapeElement.RECTANGLE);
        elementTrackOff.setRgbColor(RgbColor.fromArgbInt(0xFF808080));
        elementTrackOff.setCornerRadius(SWITCH_CORNER_RADIUS);
        switchStyle.setTrackElement(trackElementInit(elementTrackOn, elementTrackOff));
        switchStyle.setThumbElement(thumbElementInit(elementThumbOn, elementThumbOff));
    }

    private void showListPop() {
        ListContainer spinnerListView = new ListContainer(getContext());
        ToolbarAdapter spinnerAdapter = new ToolbarAdapter(getContext(), getPassTypeData());
        spinnerListView.setItemProvider(spinnerAdapter);
        spinnerListView.setItemClickedListener((parent, view, position, id) -> {
            if (mPopupWindow != null) {
                mPopupWindow.destroy();
                mPopupWindow = null;
            }
            mText.setText(getPassTypeData().get(position));
            if ("pattern_number".equals(mText.getText())) {
                mGridPasswordView3.setPasswordType(PasswordType.NUMBER);
            } else if ("pattern_password".equals(mText.getText())) {
                mGridPasswordView3.setPasswordType(PasswordType.PASSWORD);
            } else if ("pattern_text".equals(mText.getText())) {
                mGridPasswordView3.setPasswordType(PasswordType.TEXT);
            }
        });
        mPopupWindow = new PopupDialog(getContext(), mText);
        mPopupWindow.setCustomComponent(spinnerListView);
        mPopupWindow.show();
        mPopupWindow.setDialogListener(() -> {
            if (mPopupWindow != null) {
                mPopupWindow.destroy();
                mPopupWindow = null;
            }
            return false;
        });
    }

    /**
     * 获取密码类型数据
     *
     * @return Spinner数据
     */
    private List<String> getPassTypeData() {
        ArrayList<String> menuList = new ArrayList<>();
        menuList.add("pattern_number");
        menuList.add("pattern_password");
        menuList.add("pattern_text");
        return menuList;
    }
}
