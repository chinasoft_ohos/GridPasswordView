## 1.0.0
* 正式版发布

## 0.0.4-SNAPSHOT
ohos 第四个版本，基本实现了原库的功能

* 根据findbugs中的建议修改
* 修改README.md

## 0.0.3-SNAPSHOT
ohos 第三个版本，基本实现了原库的功能

* 修改README.md

## 0.0.2-SNAPSHOT
ohos 第二个版本，基本实现了原库的功能

* 代码优化

## 0.0.1-SNAPSHOT
ohos 第一个版本，基本实现了原库的功能

* 原组件中密码输入功能基本完成，类似于微信应用和支付宝应用中的支付密码视图
* 系统软键盘无法拉起，未能找到控制系统软键盘的Api
* 部分继承自源码的方法未实现，原因：1、OpenHarmony不支持这些Api；2、这些方法在原项目中只是用于空实现，没有实际意义；如这些方法：setBackground、 setBackgroundColor、 setBackgroundResource、 setBackgroundDra等。